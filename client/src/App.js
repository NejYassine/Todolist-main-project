import { useState, useEffect } from 'react';
import axios from "axios";

import './app.css';
import Item from './components/Item'

function App() {

  const [text, setText] = useState("");
  const [todo, setTodo] = useState([]);
  const [isUpdating, setUpdating] = useState("");

  useEffect(() => {
    axios.get("http://192.168.49.2:30001/get-todo")

    // j'ai remplacé cette adresse : http://localhost:5000/ par l'ip et le port du projet
      .then((res) => setTodo(res.data))
      .catch((err) => console.log(err));
  })

  const addUpdateTodo = () => {

    if (isUpdating === "") {
      axios.post("http://192.168.49.2:30001/save-todo", { text })
          // j'ai remplacé cette adresse : http://localhost:5000/ par l'ip et le port du projet

        .then((res) => {
          console.log(res.data);
          setText("");
        })
        .catch((err) => console.log(err));
    }else{
      axios.post("http://192.168.49.2:30001/update-todo", { _id: isUpdating, text })
          // j'ai remplacé cette adresse : http://localhost:5000/ par l'ip et le port du projet

        .then((res) => {
          console.log(res.data);
          setText("");
          setUpdating("");
        })
        .catch((err) => console.log(err));
    }
  }

  const deleteTodo = (_id) => {
    axios.post("http://192.168.49.2:30001/delete-todo", { _id })
        // j'ai remplacé cette adresse : http://localhost:5000/ par l'ip et le port du projet

      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
  }

  const updateTodo = (_id, text) => {
    setUpdating(_id);
    setText(text);
  }

  return (
    <div className="App">
      <div className="container">
        <h1>ToDo App</h1>
        <div className="top">
          <input
            type="text"
            placeholder='Write Something...'
            value={text}
            onChange={(e) => setText(e.target.value)} />
          <div className="add"
            onClick={addUpdateTodo}>{isUpdating ? "Update" : "Add"}</div>
        </div>

        <div className="list">
          {todo.map(item => <Item
            key={item._id}
            text={item.text}
            remove={() => deleteTodo(item._id)}
            update={() => updateTodo(item._id, item.text)} />)}
        </div>

      </div>
    </div>
  );
}

export default App;
